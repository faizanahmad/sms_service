<?php

namespace AppBundle\Command;

use AppBundle\Entity\SmsProcessor;
use Doctrine\DBAL\LockMode;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class CaribaSendSmsCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('cariba:send-sms')
            ->setDescription('...')
            ->addArgument('argument', InputArgument::OPTIONAL, 'Argument description')
            ->addOption('option', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     * @throws \Doctrine\DBAL\ConnectionException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\TransactionRequiredException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $argument = $input->getArgument('argument');

        if ($input->getOption('option')) {
            // ...
        }

        $twilioService = $this->getContainer()->get('twilio_service');

        $em = $this->getContainer()->get('doctrine.orm.entity_manager');

        //Results per page
        $pageSize = 100;

        $query = $em->getRepository(SmsProcessor::class)->getPaginatorQuery();
        $paginator = new Paginator($query);

        $totalItems = count($paginator);
        $totalPages = ceil($totalItems / $pageSize);


        for($page = 1; $page <= $totalPages; $page++) {
            $output->writeln("Page: ".$page);

            $paginator->getQuery()->setFirstResult(0)->setMaxResults($pageSize);
            foreach($paginator as $smsObject) {

                $em->getConnection()->beginTransaction();

                try {

                    //Take a write lock on entity
                    $em->find(SmsProcessor::class, $smsObject->getId(), LockMode::PESSIMISTIC_WRITE, 1);

                    $phoneNumber = $smsObject->getPhoneNumber();
                    $msg = $smsObject->getMsg();

                    //check if object is already at same version
                    $smsObjectCopy = $em->getRepository(SmsProcessor::class)->find($smsObject->getId());
                    if($smsObjectCopy->getVersion() != 1) {
                        $em->getConnection()->rollBack();
                        throw new \Exception($smsObjectCopy->getId().": SMS already sent by other command");
                    }

                    $smsObject->setIsProcessed(true);
                    $em->persist($smsObject);
                    $em->flush();
                    $em->getConnection()->commit();

                    //Send SMS
                    $result = $twilioService->sendSms($phoneNumber, $msg);
                    $smsObject->setResult($result);
                    $em->persist($smsObject);
                    $em->flush();


                    $output->writeln($smsObject->getId() . ': Sending SMS to: ' . $phoneNumber);

                }
                catch (ORMException $e) {
                    $em->getConnection()->rollBack();
                    $output->writeln("ORM Exception: ".$e->getMessage());
                }
                catch(\Exception $e) {
                    $output->writeln($e->getMessage());
                }
            }
        }
    }
}
