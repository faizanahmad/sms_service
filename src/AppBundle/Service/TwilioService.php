<?php

namespace AppBundle\Service;

use Twilio\Rest\Client;

class TwilioService
{
    private $twilio_sid;
    private $twilio_token;
    private $twilio_from;

    public function __construct($twilio_sid, $twilio_token, $twilio_from)
    {
        $this->twilio_sid = $twilio_sid;
        $this->twilio_token = $twilio_token;
        $this->twilio_from = $twilio_from;

    }
    public function sendSms($phoneNumber, $msg)
    {
        try {
            $twilioClient = new Client($this->twilio_sid, $this->twilio_token);
            $message = $twilioClient->messages->create($phoneNumber,array('from'=>$this->twilio_from, 'body'=>$msg));
            return 'success';
        }
        catch (\Exception $e){
            return $e->getMessage();
        }

    }
}