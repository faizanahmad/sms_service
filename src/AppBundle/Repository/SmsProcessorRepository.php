<?php

namespace AppBundle\Repository;

/**
 * SmsProcessorRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class SmsProcessorRepository extends \Doctrine\ORM\EntityRepository
{
    public function getPaginatorQuery()
    {
        $qb = $qb = $this->createQueryBuilder('sms')
            ->where('sms.isProcessed = 0')
            ->getQuery();
        return $qb;
    }

}
